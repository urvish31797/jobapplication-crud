var form = $("form[name='job-application-form']");
var save_url = base_url+ '/job-application/save';
var update_url = base_url + '/home';

function save() {

    if (form.valid()) {
      var formData = new FormData(form[0]);
      $.ajax({
        url: save_url,
        type: 'POST',
        processData: false,
        contentType: false,
        cache: false,
        data: formData,
        success: function (result) {
          toastr.success(result.message);
          setTimeout(() => {
            window.location.reload();
          }, 1200);
        },
        error: function (jqXHR) {
          if (jqXHR.status == 400) {
            toastr.error(jqXHR.responseJSON.message);
          }
          else {
            toastr.error("Something went wrong!");
          }
        }
      });
    }
    else{
      toastr.error("Please fillup all required fields!");
    }
}

function update() {

  if (form.valid()) {
    var formData = new FormData(form[0]);
    $.ajax({
      url: update_url,
      type: 'POST',
      processData: false,
      contentType: false,
      cache: false,
      data: formData,
      success: function (result) {
        toastr.success(result.message);
        setTimeout(() => {
          window.location = update_url;
        }, 1200);
      },
      error: function (jqXHR) {
        if (jqXHR.status == 400) {
          toastr.error(jqXHR.responseJSON.message);
        }
        else {
          toastr.error("Something went wrong!");
        }
      }
    });
  }
  else{
    toastr.error("Please fillup all required fields!");
  }
}

function add_experience()
{
  var lst = $('#exp-lists').children().last().attr('id');
  var lstval = lst.split("-");

  var prev_index = lstval[1];
  var prev_company = $("#company_name"+prev_index).val();
  var prev_designation = $("#company_designation"+prev_index).val();
  var prev_from_date = $("#from_date"+prev_index).val();
  var prev_to_date = $("#to_date"+prev_index).val();
  
  if(prev_company == "" || prev_designation == "" || prev_from_date == "" || prev_to_date == "")
  {
    toastr.error("Please fillup all fields of work experience!");
    return;
  }

  var filescount = parseInt(prev_index)+1;

  var htmldiv = '<div class="form-row all_experiences" id="experience-'+filescount+'">'+
                  '<div class="col-md-3 form-group">'+
                    '<label>Company Name</label>'+
                    '<input type="text" class="form-control" id="company_name'+filescount+'" name="jwe_company_name[]">'+
                  '</div>'+
                  '<div class="col-md-3 form-group">'+
                    '<label>Designation</label>'+
                    '<input type="text" class="form-control" id="company_designation'+filescount+'" name="jwe_company_designation[]">'+
                  '</div>'+
                  '<div class="col-md-2 form-group">'+
                    '<label>From Date</label>'+
                    '<input type="date" class="form-control" id="from_date'+filescount+'" name="jwe_company_from_date[]">'+
                  '</div>'+
                  '<div class="col-md-2 form-group">'+
                    '<label>To Date</label>'+
                    '<input type="date" class="form-control" id="to_date'+filescount+'" name="jwe_company_to_date[]">'+
                  '</div>'+
                  '<div class="col-md-2 form-group">'+
                     '<button type="button" onclick="remove_experience('+filescount+')" class="margin-top-35px btn btn-danger btn-sm"><i class="fa fa-times"></i></button>'+
                  '</div>'+
                '</div>';
  
  $("#exp-lists").append(htmldiv);
}

function remove_experience(id){
  if($('.all_experiences').length > 1)
  {
    $("#experience-"+id).remove();
  }
  else{
    alert('atleast one entry is required');
  }
}

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});