@extends('layouts.app')
@section('title', __('Job Applications'))
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="table-responsive">
                {{$dt_html->table(['class'=>"job_applications_listing display responsive","width"=>"100%"],true)}}
            </div>
        </div>
    </div>
    <input type="hidden" id="did">
</div>
<script>
var delete_url = base_url + "/home";

function delete_job(){
    var did = $("#did").val();
    $.ajax({
        url: delete_url + "/" + did ,
        type: 'DELETE',
        processData: false,
        contentType: false,
        cache: false,
        success: function (result) {
          toastr.success(result.message);
          $('.degree_listing').DataTable().ajax.reload();
          window.location.reload();
        },
        error: function (jqXHR) {
          toastr.error("Something went wrong!");
        }
      });
}

function triggerDelete(id){
    $("#did").val(id);
    
    $.confirm({
        title: 'Delete!',
        content: 'Are you sure want to delete this entry!',
        buttons: {
            confirm: function () {
                delete_job();
            },
            cancel: function () {
                
            }
        }
    });
}

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
</script>

@endsection
@push('custom-scripts')
{!! $dt_html->scripts() !!}
@endpush