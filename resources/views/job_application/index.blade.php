@extends('layout.app_without_login')
@section('title', __('Job Applications'))
@section('page_name', __('Job Applications Listings'))
@section('content')
<div class="row">
    <div class="col-12">
        <div class="table-responsive">
            {{$dt_html->table(['class'=>"job_applications_listing display responsive","width"=>"100%"],true)}}
        </div>
    </div>
</div>
@endsection
@push('custom-scripts')
<!-- Include datatable Page JS -->
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/dashboard/job_application.js') }}"></script>
{!! $dt_html->scripts() !!}
{!! JsValidator::formRequest(\App\Http\Requests\JobApplicationRequest::class, '#job-application-form'); !!}
@endpush