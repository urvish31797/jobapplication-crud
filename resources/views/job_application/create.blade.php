@extends('layout.app_without_login')
@section('title', __('Apply Job Application'))
@section('page_name', __('Job Application Form'))
@section('content')
<div class="row justify-content-center">
   <div class="col-md-12">
      <div class="card">
         <header class="card-header">
            <!-- <a href="" class="float-right btn btn-outline-primary mt-1">Log in</a> -->
            <h4 class="card-title mt-2">Basic Detail</h4>
         </header>
         <form id="job-application-form" name="job-application-form" autocomplete="off">
            <article class="card-body">
               <div class="form-row">
                  <div class="col form-group">
                     <label>First name *</label>   
                     <input type="text" class="form-control" name="joa_first_name" id="joa_first_name">
                  </div>
                  <div class="col form-group">
                     <label>Last name *</label>
                     <input type="text" class="form-control" name="joa_last_name" id="joa_last_name">
                  </div>
               </div>
               <div class="form-row">
                  <div class="col form-group">
                     <label>Designation *</label>
                     <input type="email" class="form-control" name="joa_designation" id="joa_designation">
                  </div>
                  <div class="col form-group">
                     <label>Email address *</label>
                     <input type="email" class="form-control" name="joa_email" id="joa_email">
                  </div>
               </div>
               <div class="form-row">
                  <div class="col form-group">
                     <label>Address 1 *</label>
                     <textarea class="form-control" rows="3" name="joa_first_address" id="joa_first_address"></textarea>
                  </div>
                  <div class="col form-group">
                     <label>Address 2</label>
                     <textarea class="form-control" rows="3" name="joa_second_address" id="joa_second_address"></textarea>
                  </div>
               </div>
               <div class="form-row">
                  <div class="form-group col-md-6">
                     <label>State *</label>
                     <select id="joa_state" name="joa_state" class="form-control">
                        <option value="" selected="selected"> Choose...</option>
                        <option value="Gujarat">Gujarat</option>
                        <option value="Mumbai">Mumbai</option>
                        <option value="Maharashtra">Maharashtra</option>
                        <option value="Madhya Pradesh">Madhya Pradesh</option>
                     </select>
                  </div>
                  <div class="form-group col-md-6">
                     <label>City *</label>
                     <input type="text" class="form-control" name="joa_city" id="joa_city">
                  </div>
               </div>
               <div class="form-row">
                  <div class="form-group col-md-6">
                     <label>Gender *</label>
                     <label class="form-check form-check-inline">
                     <input class="form-check-input" type="radio" name="joa_gender" value="1">
                     <span class="form-check-label"> Male </span>
                     </label>
                     <label class="form-check form-check-inline">
                     <input class="form-check-input" type="radio" name="joa_gender" value="2">
                     <span class="form-check-label"> Female</span>
                     </label>
                  </div>
                  <div class="form-group col-md-6">
                     <label>Zipcode *</label>
                     <input maxlength="6" class="form-control" type="text" name="joa_zipcode" id="joa_zipcode">
                  </div>
               </div>
               <div class="form-row">
                  <div class="form-group col-md-6">
                     <label>Relationship Status *</label>
                     <select name="joa_relation_status" id="joa_relation_status" class="form-control">
                     <option value="" selected="selected"> Choose...</option>
                        <option value="1">Single</option>
                        <option value="2">Married</option>
                     </select>
                  </div>
                  <div class="form-group col-md-6">
                     <label>Date of Birth *</label>
                     <input class="form-control" type="date" name="joa_date_of_birth" id="joa_date_of_birth">
                  </div>
               </div>
            </article>
            <br>
            <header class="card-header">
               <h4 class="card-title mt-2">Education Detail</h4>
            </header>
            <article class="card-body">
               <div class="form-row">
                  <div class="col form-group">
                     <label>SSC Result </label>
                  </div>
               </div>
               <div class="form-row">
                  <div class="col-md-4 form-group">
                     <label>Name Of Board</label>
                     <input type="text" class="form-control" name="joa_ssc_board" id="joa_ssc_board">
                  </div>
                  <div class="col-md-4 form-group">
                     <label>Passing Year</label>
                     <input type="text" class="form-control" name="joa_ssc_passing_year" id="joa_ssc_passing_year">
                  </div>
                  <div class="col-md-4 form-group">
                     <label>Percentage</label>
                     <input type="text" class="form-control" name="joa_ssc_percentage" id="joa_ssc_percentage">
                  </div>
               </div>
               <hr>
               <div class="form-row">
                  <div class="col form-group">
                     <label>HSC/Diploma Result </label>
                  </div>
               </div>
               <div class="form-row">
                  <div class="col-md-4 form-group">
                     <label>Name Of Board</label>
                     <input type="text" class="form-control" name="name_of_board_hsc_result">
                  </div>
                  <div class="col-md-4 form-group">
                     <label>Passing Year</label>
                     <input type="text" class="form-control" name="passing_year_hsc_result">
                  </div>
                  <div class="col-md-4 form-group">
                     <label>Percentage</label>
                     <input type="text" class="form-control" name="percentage_hsc_result">
                  </div>
               </div>
               <hr>
               <div class="form-row">
                  <div class="col form-group">
                     <label>BE Result </label>
                  </div>
               </div>
               <div class="form-row">
                  <div class="col-md-3 form-group">
                     <label>Name Of Board</label>
                     <input type="text" class="form-control" name="name_of_board_be_result">
                  </div>
                  <div class="col-md-3 form-group">
                     <label>University</label>
                     <input type="text" class="form-control" name="university_be_result">
                  </div>
                  <div class="col-md-3 form-group">
                     <label>Passing Year</label>
                     <input type="text" class="form-control" name="passing_year_be_result">
                  </div>
                  <div class="col-md-3 form-group">
                     <label>Percentage</label>
                     <input type="text" class="form-control" name="percentage_be_result">
                  </div>
               </div>
               <hr>
               <div class="form-row">
                  <div class="col form-group">
                     <label>Master Degree Result </label>
                  </div>
               </div>
               <div class="form-row">
                  <div class="col-md-3 form-group">
                     <label>Name Of Board</label>
                     <input type="text" class="form-control" name="name_of_board_me_result">
                  </div>
                  <div class="col-md-3 form-group">
                     <label>University</label>
                     <input type="text" class="form-control" name="university_me_result">
                  </div>
                  <div class="col-md-3 form-group">
                     <label>Passing Year</label>
                     <input type="text" class="form-control" name="passing_year_me_result">
                  </div>
                  <div class="col-md-3 form-group">
                     <label>Percentage</label>
                     <input type="text" class="form-control" name="percentage_me_result">
                  </div>
               </div>
            </article>
            <header class="card-header">
               <h4 class="card-title mt-2">Work Experience</h4>
            </header>
            <article class="card-body">
               <div id="exp-lists">
                  <div class="form-row all_experiences" id="experience-0">
                     <div class="col-md-3 form-group">
                        <label>Company Name</label>
                        <input type="text" class="form-control" name="jwe_company_name[]" id="company_name0">
                     </div>
                     <div class="col-md-3 form-group">
                        <label>Designation</label>
                        <input type="text" class="form-control" name="jwe_company_designation[]" id="company_designation0">
                     </div>
                     <div class="col-md-2 form-group">
                        <label>From Date</label>
                        <input type="date" class="form-control" name="jwe_company_from_date[]" id="from_date0">
                     </div>
                     <div class="col-md-2 form-group">
                        <label>To Date</label>
                        <input type="date" class="form-control" name="jwe_company_to_date[]" id="to_date0">
                     </div>
                  </div>
               </div>
               <br>
               <button type="button" onclick="add_experience()" class="float-left btn btn-danger btn-sm"><i class="fa fa-plus"></i> Add Experience</button>
               <br>
            </article>
            <header class="card-header">
               <h4 class="card-title mt-2">Languages Known</h4>
            </header>
            <article class="card-body com-md-6">
               <div class="form-group">
                  <label>Hindi</label>&nbsp;&nbsp;&nbsp;
                  <input type="checkbox">&nbsp;&nbsp;Read
                  <input type="checkbox">&nbsp;&nbsp;Write
                  <input type="checkbox">&nbsp;&nbsp;Speak
               </div>
               <div class="form-row">
                  <div class="form-group">
                     <label>English</label>&nbsp;&nbsp;&nbsp;
                     <input type="checkbox">&nbsp;&nbsp;Read
                     <input type="checkbox">&nbsp;&nbsp;Write
                     <input type="checkbox">&nbsp;&nbsp;Speak
                  </div>
               </div>
               <div class="form-row">
                  <div class="form-group">
                     <label>Gujarati</label>&nbsp;&nbsp;&nbsp;
                     <input type="checkbox">&nbsp;&nbsp;Read
                     <input type="checkbox">&nbsp;&nbsp;Write
                     <input type="checkbox">&nbsp;&nbsp;Speak
                  </div>
               </div>
               <br>
            </article>
            <header class="card-header">
               <h4 class="card-title mt-2">Technologies you know</h4>
            </header>
            <article class="card-body com-md-6">
               <div class="form-group">
                  <label>PHP</label>&nbsp;&nbsp;&nbsp;
                  <input name="technology_php" value="Begineer" type="radio">&nbsp;&nbsp;Begineer
                  <input name="technology_php" value="Meadator" type="radio">&nbsp;&nbsp;Meadator
                  <input name="technology_php" value="Expert" type="radio">&nbsp;&nbsp;Expert
               </div>
               <div class="form-row">
                  <div class="form-group">
                     <label>MySql</label>&nbsp;&nbsp;&nbsp;
                     <input name="technology_mysql" value="Begineer" type="radio">&nbsp;&nbsp;Begineer
                  <input name="technology_meadator" value="Meadator" type="radio">&nbsp;&nbsp;Meadator
                  <input name="technology_expert" value="Expert" type="radio">&nbsp;&nbsp;Expert
                  </div>
               </div>
               <div class="form-row">
                  <div class="form-group">
                     <label>Laravel</label>&nbsp;&nbsp;&nbsp;
                     <input name="technology_laravel" value="Begineer" type="radio">&nbsp;&nbsp;Begineer
                  <input name="technology_laravel" value="Meadator" type="radio">&nbsp;&nbsp;Meadator
                  <input name="technology_laravel" value="Expert" type="radio">&nbsp;&nbsp;Expert
                  </div>
               </div>
               <div class="form-row">
                  <div class="form-group">
                     <label>Oracle</label>&nbsp;&nbsp;&nbsp;
                     <input name="technology_oracle" value="Begineer" type="radio">&nbsp;&nbsp;Begineer
                  <input name="technology_oracle" value="Meadator" type="radio">&nbsp;&nbsp;Meadator
                  <input name="technology_oracle" value="Expert" type="radio">&nbsp;&nbsp;Expert
                  </div>
               </div>
               <br>
            </article>
            <header class="card-header">
               <h4 class="card-title mt-2">Reference Contact</h4>
            </header>
            <article class="card-body com-md-6">
               <div class="form-row">
                  <div class="col-md-4 form-group">
                     <label>Name</label>
                     <input type="text" class="form-control" name="joa_first_reference_name" id="joa_first_reference_name">
                  </div>
                  <div class="col-md-4 form-group">
                     <label>Contact Number</label>
                     <input type="phone" maxlength="10" class="form-control" name="joa_first_reference_number" id="joa_first_reference_number">
                  </div>
                  <div class="col-md-4 form-group">
                     <label>Relation</label>
                     <input type="text" class="form-control" name="joa_first_reference_relation" id="joa_first_reference_relation">
                  </div>
               </div>
               <div class="form-row">
                  <div class="col-md-4 form-group">
                     <label>Name</label>
                     <input type="text" class="form-control" name="name_of_board" id="joa_second_reference_name">
                  </div>
                  <div class="col-md-4 form-group">
                     <label>Contact Number</label>
                     <input type="phone" maxlength="10" class="form-control" name="passing_year" id="joa_second_reference_number">
                  </div>
                  <div class="col-md-4 form-group">
                     <label>Relation</label>
                     <input type="text" class="form-control" name="percentage" id="joa_second_reference_relation">
                  </div>
               </div>
               <br>
            </article>
            <header class="card-header">
               <h4 class="card-title mt-2">Prefrances</h4>
            </header>
            <article class="card-body">
               <div class="form-row">
                  <div class="form-group col-md-6">
                     <label>Prefered Location *</label>
                     <select name="joa_location" id="joa_location" class="form-control">
                     <option value="" selected="selected"> Choose...</option>
                        <option value="1">Work From Home</option>
                        <option value="2">Office</option>
                     </select>
                  </div>
                  <div class="form-group col-md-6">
                     <label>Department *</label>
                     <select name="joa_department" id="joa_department" class="form-control">
                     <option value="" selected="selected"> Choose...</option>
                        <option value="1">Design</option>
                        <option value="2">Development</option>
                     </select>
                  </div>
               </div>
               <div class="form-row">
                  <div class="form-group col-md-4">
                     <label>Notice Period *</label>
                     <input class="form-control" type="text" name="joa_notice_period" id="joa_notice_period">
                  </div>
                  <div class="form-group col-md-4">
                     <label>Expected CTC *</label>
                     <input class="form-control" maxlength="8" type="number" name="joa_expected_ctc" id="joa_expected_ctc">
                  </div>
                  <div class="form-group col-md-4">
                     <label>Current CTC *</label>
                     <input class="form-control" maxlength="8" type="number" name="joa_current_ctc" id="joa_current_ctc">
                  </div>
               </div>
               <div class="form-row">
                  <div class="col form-group">
                     <button type="button" onclick="save()" class="btn btn-primary btn-block"> Submit  </button>
                  </div>
                  <div class="col form-group">
                     <a href="{{route('job-application.create')}}" class="btn btn-info btn-block"> Cancel  </a>
                  </div>
               </div>
            </article>
         </form>
      </div>
   </div>
</div>
@endsection
@push('custom-scripts')
<!-- Include datatable Page JS -->
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/page/job_application.js') }}"></script>
{!! JsValidator::formRequest(\App\Http\Requests\JobApplicationRequest::class, '#job-application-form'); !!}
@endpush