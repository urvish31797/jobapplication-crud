<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobWorkExperienceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_work_experience', function (Blueprint $table) {
            $table->integer('pkJwe')->autoIncrement();
            $table->integer('fkJweJoa')->nullable();
            $table->string('jwe_company_name', 30)->nullable();
            $table->string('jwe_company_designation', 30)->nullable();
            $table->date('jwe_company_from_date', 10)->nullable();
            $table->date('jwe_company_to_date', 10)->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->softDeletes('deleted_at', 0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_work_experience');
    }
}
