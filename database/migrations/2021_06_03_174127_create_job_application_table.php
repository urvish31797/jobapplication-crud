<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobApplicationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_application', function (Blueprint $table) {
            $table->integer('pkJoa')->autoIncrement();
            $table->string('joa_first_name', 15)->nullable();
            $table->string('joa_last_name', 15)->nullable();
            $table->string('joa_designation', 30)->nullable();
            $table->string('joa_email', 30)->nullable();
            $table->string('joa_first_address', 255)->nullable();
            $table->string('joa_second_address', 255)->nullable();
            $table->string('joa_state', 20)->nullable();
            $table->string('joa_city', 30)->nullable();
            $table->string('joa_zipcode', 6)->nullable();
            $table->tinyInteger('joa_relation_status')->nullable();
            $table->tinyInteger('joa_gender')->nullable();
            $table->date('joa_date_of_birth')->nullable();
            $table->tinyInteger('joa_location')->nullable();
            $table->tinyInteger('joa_department')->nullable();
            $table->string('joa_notice_period', 10)->nullable();
            $table->float('joa_expected_ctc')->nullable();
            $table->float('joa_current_ctc')->nullable();
            $table->string('joa_first_reference_name', 20)->nullable();
            $table->string('joa_first_reference_number', 10)->nullable();
            $table->string('joa_first_reference_relation', 10)->nullable();
            $table->string('joa_second_reference_name', 20)->nullable();
            $table->string('joa_second_reference_number', 10)->nullable();
            $table->string('joa_second_reference_relation', 10)->nullable();
            $table->string('joa_ssc_board', 30)->nullable();
            $table->string('joa_ssc_passing_year', 30)->nullable();
            $table->string('joa_ssc_percentage', 30)->nullable();
            $table->string('joa_hsc_board', 30)->nullable();
            $table->string('joa_hsc_passing_year', 30)->nullable();
            $table->string('joa_hsc_percentage', 30)->nullable();
            $table->string('joa_be_board', 30)->nullable();
            $table->string('joa_be_university', 30)->nullable();
            $table->string('joa_be_passing_year', 30)->nullable();
            $table->string('joa_be_percentage', 30)->nullable();
            $table->string('joa_me_board', 30)->nullable();
            $table->string('joa_me_university', 30)->nullable();
            $table->string('joa_me_passing_year', 30)->nullable();
            $table->string('joa_me_percentage', 30)->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_application');
    }
}
