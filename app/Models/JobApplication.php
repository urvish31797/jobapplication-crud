<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobApplication extends Model
{
    use HasFactory,SoftDeletes;
    protected $table = 'job_application';
    public $timestamps = true;
    protected $primaryKey = 'pkJoa';
    protected $guarded = ['pkJoa'];
    public $appends = ['joa_full_name'];

    public static function boot()
    {
        parent::boot();
        self::deleting(function ($q) { // before delete() method call this
            $q->job_work_experience()->each(function ($experience) {
                $experience->delete(); // <-- direct deletion
            });
            // do the rest of the cleanup...
        });
    }
    
    public function job_work_experience()
    {
        return $this->hasMany(JobWorkExperience::class, 'fkJweJoa', 'pkJoa');
    }
    
    public function getJoaGenderAttribute($value)
    {
        if($value == 1){
            $gender = "Male";
        }
        else{
            $gender = "Female";
        }
        return $gender;
    }

    public function setJoaDateOfBirthAttribute($value)
    {
        $this->attributes['joa_date_of_birth'] = Carbon::parse($value)->format('Y-m-d');
    }

    public function getJoaFullNameAttribute()
    {
        return $this->attributes['joa_full_name'] = "{$this->joa_first_name} {$this->joa_last_name}";
    }
}