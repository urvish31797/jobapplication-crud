<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class JobWorkExperience extends Model
{
    use HasFactory,SoftDeletes;
    protected $table = 'job_work_experience';
    public $timestamps = true;
    protected $primaryKey = 'pkJwe';
    protected $guarded = ['pkJwe'];
}
