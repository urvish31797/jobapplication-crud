<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\Html\Builder;
use App\Http\Requests\JobApplicationRequest;
use App\Models\JobApplication;
use App\Models\JobWorkExperience;
use HttpResponse;
use Carbon\Carbon;

class JobApplicationController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('job_application.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(JobApplicationRequest $request)
    {
        $count = JobApplication::where('joa_email',$request->joa_email)->count();
        if(!$count)
        {   
            try{
                $id = JobApplication::insertGetId($request->validated());
        
                if (!empty($id)) {
                    if(count($request->jwe_company_name))
                    {
                        for($i=0;$i<count($request->jwe_company_name);$i++)
                        {
                            $row['fkJweJoa'] = $id;
                            $row['jwe_company_name'] = $request->jwe_company_name[$i];
                            $row['jwe_company_designation'] = $request->jwe_company_designation[$i];
                            $row['jwe_company_from_date'] = Carbon::parse($request->jwe_company_from_date[$i])->format('Y-m-d');
                            $row['jwe_company_to_date'] = Carbon::parse($request->jwe_company_to_date[$i])->format('Y-m-d');
                            $experience_data[] = $row;
                        }

                        JobWorkExperience::insert($experience_data);
                    }
                    $response['message'] = __('Your application is submitted successfully!');
                } else {
                    $status_code = HttpResponse::HTTP_BAD_REQUEST;
                    $response['message'] = __('Sorry! Your application is not submitted successfully');
                }
            }
            catch(\Exception $e){
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = __('DB Error!');    
            }
        }
        else
        {
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
            $response['message'] = __('Sorry! You have applied already using this email');
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }
}
