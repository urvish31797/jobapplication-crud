<?php

namespace App\Http\Controllers;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\Html\Builder;
use App\Http\Requests\JobApplicationRequest;
use App\Models\JobApplication;
use App\Models\JobWorkExperience;
use Datatables;
use HttpResponse;
use HtmlBuilder;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(HtmlBuilder $builder)
    {
        $columns = [
            ['data' => 'DT_RowIndex', 'name' => 'DT_RowIndex', 'title' => __('Sr. No'), 'render' => null, 'orderable' => false, 'searchable' => false],
            ['data' => 'joa_full_name', 'name' => 'joa_full_name', 'title' => __('Name'), 'orderable' => false],
            ['data' => 'joa_email', 'name' => 'joa_email', 'title' => __('Email')],
            ['data' => 'joa_gender', 'name' => 'joa_gender', 'title' => __('Gender')],
            ['data' => 'joa_state', 'name' => 'joa_state', 'title' => __('State')],
            ['data' => 'joa_city', 'name' => 'joa_city', 'title' => __('City')],
            ['data' => 'jwe_company_name', 'name' => 'jwe_company_name', 'title' => __('Last Company'), 'orderable' => false],
            ['data' => 'action', 'name' => 'action', 'title' => __('Actions'), 'orderable' => false, 'searchable' => false, 'width' => '10%'],
        ];

        $ajax = [
            'url'=> route('job-applications.list'),
            'data' => 'function(d) {
                d.search =  $("#search_application").val();
            }',
            'type' => 'POST'
        ];

        $dt_html = $builder->addIndex()
        ->columns($columns)
        ->ajax($ajax)
        ->parameters([
            'processing' => false,
            'searching' => true,
        ]);

        return view('admin.job_application.index',compact('dt_html'));
    }

    public function show($id)
    {
        $data = JobApplication::with(['job_work_experience'])->findorFail($id);
        return view('admin.job_application.view',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = JobApplication::with(['job_work_experience'])->findorFail($id);
        return view('admin.job_application.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function store(JobApplicationRequest $request)
    {
        if(!empty($request->pkJoa))
        {
            $count = JobApplication::where('pkJoa','!=',$request->pkJoa)->where('joa_email',$request->joa_email)->count();
            if(!$count)
            {
                $status = JobApplication::find($request->pkJoa)->update($request->validated());
                if (!empty($status)) {
                    $response['message'] = __('Your application is updated successfully!');

                    //delete old entries
                    JobWorkExperience::where('fkJweJoa',$request->pkJoa)->delete();

                    //insert new items
                    if(count($request->jwe_company_name))
                    {
                        for($i=0;$i<count($request->jwe_company_name);$i++)
                        {
                            $row['fkJweJoa'] = $request->pkJoa;
                            $row['jwe_company_name'] = $request->jwe_company_name[$i];
                            $row['jwe_company_designation'] = $request->jwe_company_designation[$i];
                            $row['jwe_company_from_date'] = Carbon::parse($request->jwe_company_from_date[$i])->format('Y-m-d');
                            $row['jwe_company_to_date'] = Carbon::parse($request->jwe_company_to_date[$i])->format('Y-m-d');

                            if(!empty($row['jwe_company_name']) && !empty($row['jwe_company_designation']) && !empty($row['jwe_company_from_date']) && !empty($row['jwe_company_to_date'])){
                                $experience_data[] = $row;
                            }
                        }

                        JobWorkExperience::insert($experience_data);
                    }
                } else {
                    $status_code = HttpResponse::HTTP_BAD_REQUEST;
                    $response['message'] = __('Sorry! Your application is not updated successfully');
                }
            }
            else
            {
                $status_code = HttpResponse::HTTP_BAD_REQUEST;
                $response['message'] = __('Sorry! You have applied already using this email');
            }
        }
        else{
            $status_code = HttpResponse::HTTP_BAD_REQUEST;
            $response['message'] = __('Something went wrong!');
        }

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        JobApplication::find($id)->delete();
        $response['message'] = __('Deleted Successfully!');

        return response()->json($response,$status_code ?? HttpResponse::HTTP_OK);
    }

    public function job_applications_list()
    {
        $j_app = JobApplication::with(['job_work_experience'])->select('*');

        $j_app->when(request('search'), function ($q){
            return $q->where('joa_first_name', 'LIKE', '%' . request('search') . '%')
            ->orWhere('joa_last_name', 'LIKE', '%' . request('search') . '%');
        });
        // dd($j_app->get());
        return DataTables::of($j_app)
            ->editColumn('jwe_company_name',function($j_app){
                $rev = array_reverse($j_app->job_work_experience->toArray());
                
                return $j_app->job_work_experience[0]->jwe_company_name;
            })
            ->addColumn('action', function ($j_app) {
                $str = '<a href='.url("home/".$j_app->pkJoa."/edit").'><i class="fa fa-edit"></i></a>&nbsp;&nbsp';
                $str .= '<a href='.url("home/".$j_app->pkJoa).'><i class="fa fa-eye"></i></a>&nbsp;&nbsp';
                $str .= '<a onclick="triggerDelete('.$j_app->pkJoa.')" href="javascript:void(0)"><i class="fa fa-trash"></i></a>';
                return $str;
            })
            ->addIndexColumn()
            ->escapeColumns()
            ->toJSON();
    }
}
