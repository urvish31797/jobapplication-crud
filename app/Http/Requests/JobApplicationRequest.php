<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class JobApplicationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'joa_first_name' => 'required|max:15',
            'joa_last_name' => 'required|max:15',
            'joa_designation' => 'required|max:30',
            'joa_email' => 'required|email:rfc,dns|max:30',
            'joa_first_address' => 'required|max:255',
            'joa_second_address' => 'nullable',
            'joa_state' => 'required|max:20',
            'joa_city' => 'required|max:30',
            'joa_zipcode' => 'required|numeric',
            'joa_relation_status' => 'required|max:1',
            'joa_gender' => 'required|max:1',
            'joa_date_of_birth' => 'required|date',
            'joa_location' => 'required|max:1',
            'joa_department' => 'required|max:1',
            'joa_notice_period' => 'required|max:10',
            'joa_expected_ctc' => 'required|numeric',
            'joa_current_ctc' => 'required|numeric',
            'joa_ssc_board' => 'required|max:30',
            'joa_ssc_passing_year' => 'required|max:30',
            'joa_ssc_percentage' => 'required|max:30',
            'joa_first_reference_name' => 'nullable|max:20',
            'joa_first_reference_number' => 'nullable|integer',
            'joa_first_reference_relation' => 'nullable|max:10',
            'joa_second_reference_name' => 'nullable|max:20',
            'joa_second_reference_number' => 'nullable|integer',
            'joa_second_reference_relation' => 'nullable|max:10',
        ];
    }

    public function messages()
    {
        return [
            'joa_first_name.required' => 'First name is Required',
            'joa_first_name.max' => 'First name should not be greater than :max',
            'joa_last_name.required' => 'Last name is Required',
            'joa_designation.required' => 'Designation is Required',
            'joa_email.required' => 'Email is Required',
            'joa_email.email' => 'Please enter valid email address',
            'joa_first_address.required' => 'Address is Required',
            'joa_state.required' => 'State is Required',
            'joa_city.required' => 'City is Required',
            'joa_zipcode.required' => 'Zipcode is Required',
            'joa_zipcode.numeric' => 'Zipcode must be numeric',
            'joa_relation_status.required' => 'Relation Status is Required',
            'joa_gender.required' => 'Gender is Required',
            'joa_date_of_birth.required' => 'Date of Birth is Required',
            'joa_location.required' => 'Prefered Location is Required',
            'joa_department.required' => 'Department is Required',
            'joa_notice_period.required' => 'Notice Period is Required',
            'joa_expected_ctc.required' => 'Expected CTC is Required',
            'joa_current_ctc.required' => 'Current CTC is Required',
            'joa_ssc_board' => 'Name of Board is Required',
            'joa_ssc_passing_year' => 'Passing Year is Required',
            'joa_ssc_percentage' => 'Percentage is Required',
            'joa_expected_ctc.numeric' => 'Expected CTC must be numeric',
            'joa_current_ctc.numeric' => 'Current CTC must be numeric',
        ];
    }
}
