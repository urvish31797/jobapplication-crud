<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\JobApplicationController;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes(['register' => false]);

Route::prefix('job-application')->group(function () {
    Route::get('/create',[JobApplicationController::class,'create'])->name('job-application.create');
    Route::post('/save',[JobApplicationController::class,'store'])->name('job-application.store');
});

Route::group(['middleware'=>'auth'],function(){
    Route::post('/job-applications-list', [HomeController::class,'job_applications_list'])->name('job-applications.list');
    Route::resource('/home', HomeController::class);
});

